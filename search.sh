#!/bin/bash
# Bryant Hansen

STR="$1"
if [[ ! "$1" ]] ; then
    echo "# ERROR: must provide a search term.  abort" >&2
    exit 1
fi
echo "# Search Term: $STR"

find data -name "*.txt" -exec grep -li "$STR" "{}" \; \
| sort \
| while read f ; do
    echo -e "\n\n### $f\n"
    grep -i -H -A 5 -B 5 "$STR" $f
done 2>&1
