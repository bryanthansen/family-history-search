% title Family Research
% author Bryant Hansen
% date January 2019

# Principles of Creation - Motivation, Functionality, and Structure

These are a set of scripts that fetches information from the US Government's Library of Congress and performs searches of that data.

The main script is fetch.sh, which fetches OCR-generated text data from scanned newspapers.  In the initial example, publications of [The Bottineau Conrant](https://chroniclingamerica.loc.gov/lccn/sn88076086/) from 1895 to 1923 are fetched.

As can be seen from the screen capture, not all dates are available:

![Screen Capture of LOC Web Page for Publication](doc/2019-01-25_10-45-17_1440x900b.png)

In addition, the weekly order of the Publication is not always consistent.  Over the years, the day of the week changed occasionally.  There were also a few extra editions and a few missed ones.

The script attempts to do an automated fetch of all available publications during the specified timeframe, without making an unnecessarily-large set of false requests.

Pages must be individually-requested.  On the first page error, no further pages are requested.  If there is an error fetching the first page, it's assume that the edition does not exist and the loop is aborted.

URL's for the data look like this:

            OCR/Text:  https://chroniclingamerica.loc.gov/lccn/sn88076086/1895-06-08/ed-1/seq-1/ocr/
                 PDF:  https://chroniclingamerica.loc.gov/lccn/sn88076086/1895-06-08/ed-1/seq-1.pdf
    Calendar Default:  https://chroniclingamerica.loc.gov/lccn/sn83025308/issues/
            Calendar:  https://chroniclingamerica.loc.gov/lccn/sn83025308/issues/1891/

The date can be seen formatted as _1895-06-08_ and _seq-1_ refers to page 1.

The script produces a series of wget statements and other shell statements which will perform the following steps:

 1. Create the necessary output directories
 2. Loop through a range of weekly dates and a range of pages
 3. Detect if the page has already been fetched
 4. Fetch the page
 5. Convert the html page to plain text
 6. Abort page fetches for an edition on failure
 7. Abort the entire operation if the first page fails

This is a script which produces script code.  The reason for the 2-stage approach is for ease of debugging.  One can see the exact statements that would be executed prior to performing that execution.  They can be copy-pasted into the current shell window or the output can be piped to another shell.

Typical execution:

    bash fetch.sh | bash

The other shell scripts are for searching the text and for fetching the original PDF's when hits are found.

Another branch of the project has been started for fetching The Bottineau Pioneer from 1886 to 1895.  It's not yet present due to the high level of code duplication; I have not yet made a structure to merge the functionality of both yet.

## TODO

Determine method of embedding TODO.md into the resulting documentation

## Initial 1-liners:

>  # for n in {0..100..7} ; do DT=$(date --date="1895-05-04 + $n days" +%Y-%m-%d) ; echo "mkdir $DT ; pushd $DT ; wget 
>  https://chroniclingamerica.loc.gov/lccn/sn88076086/${DT}/ed-1/seq-1/ocr/ ; popd" ; done | sed -n 1p | sh

>  # for pg in {8..10} ; do htmlf="page%02d.html" ; txtf=page%02d.txt; printf "( [[ -f $htmlf ]] || wget -O- 
>  https://chroniclingamerica.loc.gov/lccn/sn88076086/1895-05-04/ed-1/seq-%d/ocr/ > $htmlf && cat $htmlf || echo '# ERROR1' >&2 ) | [[ -f $txtf ]] || lynx -dump -stdin > $txtf\n" $pg $pg $pg $pg $pg $pg || echo "# ERROR2" >&2 ; done | sed -n 1p | sh

>  # Search
>  # find 19* -name "*.txt" -exec grep -l "Frankenstein "{}" \; | sort | while read f ; do echo -e "\n\n### $f\n" ; grep -H -A 5 -B 5 "Frankenstein" $f ; done 2>&1

