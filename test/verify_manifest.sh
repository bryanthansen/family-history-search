#!/bin/bash
# Bryant Hansen

MANIFEST=Manifest.txt
[[ "$1" ]] && MANIFEST="$1"
USAGE="$0 [ MANIFEST ]"

cat "$MANIFEST" \
| sed "s/\#.*//;/^[\ \t]*$/d" \
| while read sha file ; do
    echo -e "# Verifying $MANIFEST sha256sum of $file \n  # $sha" >&2
    filesha="$(cat "$file" | sha256sum | cut -f 1 -d ' ')"
    echo "  # ${filesha}"
    if [[ "$filesha" == "$sha" ]] ; then
        echo "  # SHA match verified"
    else
        echo -e "### ERROR: SHA MISMATCH\n" >&2
        exit 2
    fi
done
