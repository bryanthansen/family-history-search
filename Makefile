# Bryant Hansen

.PHONY: help test FORCE

help:
	@cat README.md

test: FORCE
	make -C test
