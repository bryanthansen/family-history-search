#!/bin/bash
# Bryant hansen

# Description:
# Fetch a page from the Library of Congress
# Current source is hard-coded
# PDF or TXT versions can be requested

# need a get_num_pages function to avoid false requests
# as a first step, abort on first page fetch failure

USAGE="$0 [ PDF [ <url> | <date> <page> ] | TXT timestamp page out_dir ]"

type="$1"
DT="$2"
pg="$3"

DATA_DIR=data

case "$type" in
TXT)
    # TXT timestamp page out_dir
    n_args="$#"
    case $# in
    3)
        htmlf="./page%02d.html"
        txtf='/page%02d.txt'
        url="https://chroniclingamerica.loc.gov/lccn/sn88076086/${DT}/ed-1/seq-${pg}/ocr/"
        outd="${DATA_DIR}/$DT" ;
        printf "
            if [[ ! \"\$END\" ]] ; then
                [[ -d '${outd}' ]] || mkdir -p '${outd}'
                pushd "${DATA_DIR}/${DT}" >/dev/null
                if [[ -f '$htmlf' ]] ; then
                    if [[ ! -s '$htmlf' ]] ; then
                        echo '# $htmlf is empty, indicating previous page was last'
                        END=1
                    else
                        echo '# $htmlf exists'
                        if [[ ! -f '$txtf' ]] || [[ '$htmlf' -nt '$txtf' ]] ; then
                            cat $htmlf | lynx -dump -stdin > "$txtf"
                        fi
                    fi
                else
                    if wget -O- $url > $htmlf ; then
                        cat $htmlf | lynx -dump -stdin > "$txtf"
                    else
                        END=1
                    fi
                fi
                popd >/dev/null" \
                $pg $pg $pg $pg $pg $pg $pg $pg $pg $pg $pg $pg
        printf "
                if [[ \"\$END\" ]] ; then"
                    if [[ "$pg" == 1 ]] ; then
                        # Special handing for the first page; detect if the whole edition does not exist
                        printf "
                            echo '# No delivery for $DT; suppressing further url misses: abort.'
                            exit 1"
                    else
                        printf "
                            let TOT=%d-1
                            echo \"# ${DT}: \$TOT pages\"" \
                            $pg
                    fi
        printf "
                fi
            fi
        "
        ;;
    *)
        echo "# $0 ERROR: the $type option requires 3 arguments" >&2
        echo "#  USAGE: $USAGE" >&2
        exit 3
        ;;
    esac
    ;;
PDF)
    # PDF [ <url> | <date> <page> ]"
    n_args="$#"
    case $# in
        3)
            DT="$2"
            PG="$3"
            ;;
        2)
            # example: 1895-05-04/seq-2.pdf
            # data/1895-05-04/page1.txt
            URL_FORMAT='[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]\/seq-[0-9]'
            URL_FORMAT2='[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]\/page[0-9]'
            if [[ ! "${2//*${URL_FORMAT}*/}" ]] ; then
                DT="${2%%\/seq-*}"
                DT="${DT##data\/}"
                PG="${2##*\/seq-}"
                PG="${PG%.pdf}"
                PG="${PG%.txt}"
                if [[ ! "$DT" ]] || [[ ! "$PG" ]] ; then
                    echo "# ERROR: failed to fetch parameter.  abort" >&2
                    exit 1
                fi
                echo "# url2 detected  DT=$DT  PG=$PG" >&2
            elif [[ ! "${2//*${URL_FORMAT2}*/}" ]] ; then
                DT="${2%%\/page*}"
                DT="${DT##data\/}"
                PG="${2##*\/page}"
                PG="${PG%.pdf}"
                PG="${PG%.txt}"
                if [[ ! "$DT" ]] || [[ ! "$PG" ]] ; then
                    echo "# ERROR: failed to fetch parameter.  abort" >&2
                    exit 1
                fi
                echo "# url detected  DT=$DT  PG=$PG" >&2
            else
                echo "# unknown format: '$2' (DT=$DT  PG=$PG)  abort" >&2
                exit 2
            fi
            ;;
        *)
            echo "# $0 ERROR: the $type option requires 1 or 2 arguments" >&2
            echo "#    USAGE: $USAGE" >&2
            exit 3
            ;;
    esac

    dir="${DATA_DIR}/$DT"
    url="https://chroniclingamerica.loc.gov/lccn/sn88076086/${DT}/ed-1/seq-${PG}.pdf"

    echo "# dir=$dir" >&2
    [[ -d "$dir" ]] || mkdir -p "$dir"
    if pushd "$dir" > /dev/null ; then
        b="$(basename $url)"
        d="$(basename "$PWD")"
        [[ -f "$b" ]] && echo "# data/$d/$b exists" >&2 || echo "pushd '$dir' && wget $url ; popd"
    fi
    popd > /dev/null
    ;;

-h|--help)
    echo "#  USAGE: $USAGE" >&2
    ;;

*)
    echo -e "# ERROR: type $1 unknown\n#   USAGE: $USAGE" >&2
    ;;

esac
