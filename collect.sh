#!/bin/bash
# Bryant hansen

# need a get_num_pages function
# as a first step, abort on first page fetch failure

echo -e "# To execute, pipe the output to a shell: \n#   $0 | sh"

# defaults (a test case)
START_DATE=1922-05-18
END_DATE=1923-10-15
MAX_ITERATIONS=500
TIME_UNIT=weeks
MAX_PAGES=30
SCRIPT_DIR="."

if [[ ! -f "$SCRIPT_DIR"/fetch.sh ]] ; then
    SCRIPT_DIR="$(dirname "$0")"
fi

# Allow overridig the defaults via fetch.conf
CONF="./fetch.conf"
[[ -f "$CONF" ]] && . "$CONF"

[[ "$FETCH" ]] || FETCH="$SCRIPT_DIR"/fetch.sh
if [[ ! -f "$FETCH" ]] ; then
    echo "# ERROR: failed to locate fetch $FETCH dependency; SCRIPT_DIR == $SCRIPT_DIR"
    exit 1
fi

# Do the big fetch & convert loop
# This output will be assembled shell statements
# Print statements to be executed
#   This allows an intermediate, copy-paste-testable script
#      Which can be inspected
#      or tested via copy-paste in the same environment
#      or piped to a subshell
#      or some combination

for n in $(seq 1 $MAX_ITERATIONS) ; do
    DT=$(date --date="$START_DATE + $n $TIME_UNIT" +%Y-%m-%d)
    printf "echo '# loading edition #${n}: $DT'\n"
    if [[ "$DT" > "$END_DATE" ]] ; then
        echo "# $DT exceeds end date $END_DATE  Abort" >&2
        break
    fi
    printf "unset END"
    for page in $(seq 1 $MAX_PAGES) ; do
        printf "\n# Loading date $DT page ${page} \n#    %s %s %s %s\n" \
        "$FETCH" TXT "$DT" "$page"
        "$FETCH" TXT "$DT" "$page"
        # data/1902-11-21/page02.html
    done
done
